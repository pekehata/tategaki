import { CapacitorConfig } from "@capacitor/cli"

const config: CapacitorConfig = {
  "appId": "app.tategaki.mobile",
  "appName": "tategaki",
  "webDir": process.env.CAP_ENV === "development" ? "www.dev" : "www",
  "bundledWebRuntime": false,
  "plugins": {
		"CapacitorUpdater": {
			"autoUpdate": process.env.CAP_ENV === "production"
		}
	},
  "cordova": {
    "preferences": {
      "loglevel": "DEBUG",
      "BackgroundColor": "0xffffffff",
      "ErrorUrl": "",
      "AllowInlineMediaPlayback": "false",
      "BackupWebStorage": "cloud",
      "KeyboardDisplayRequiresUserAction": "true",
      "MediaPlaybackRequiresUserAction": "false",
      "SuppressesIncrementalRendering": "false",
      "TopActivityIndicator": "gray",
      "GapBetweenPages": "0",
      "PageLength": "0",
      "PaginationBreakingMode": "page",
      "PaginationMode": "unpaginated",
      "ios-XCBuildConfiguration-TARGETED_DEVICE_FAMILY": "1,2",
      "StatusBarOverlaysWebView": "false",
      "KeepRunning": "false",
      "Orientation": "landscape",
      "SplashScreenDelay": "0",
      "Fullscreen": "true",
      "target-device": "handset",
      "DisallowOverscroll": "true",
      "EnableViewportScale": "false"
    }
  }
}

console.log(`capacitor.config.ts: Detected its mode === ${process.env.CAP_ENV}`);
if(process.env.CAP_ENV === "development") {
  console.log(`capacitor.config.ts: Adding "server" property to config...`);
  console.log(`capacitor.config.ts: Server ip address: ${process.env.IPADDR}`);
  config.server = {
    "url": `http://${process.env.IPADDR}:8080`,
    "cleartext": true
  }
}

export default config