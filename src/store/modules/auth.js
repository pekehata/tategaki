function currentPlanTranslator(currentPlan) {
  const dictionary = {
    "free": "無料プラン",
    "paid": "プレミアムプラン",
  }
  return dictionary[currentPlan] ?? '#無効なプラン#'
}

export default {
  state: {
    user: null,
    entitled: false,
    currentPlan: 'free',
    limit: {
      maxAlbums: 2,
      maxPages: 5
    }
  },
  getters: {
    currentPlan: state => currentPlanTranslator(state.currentPlan),
    limit: state => state.limit,
    entitled: state => state.entitled,
  },
  mutations: {
    changeAuthUser(state, user) {
      state.user = user
    },
    changeEntitlementStatus: (state, entitled) => {
      state.entitled = entitled
      state.currentPlan = entitled ? "paid" : "free"
    }
  }
}