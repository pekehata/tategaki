import Dexie from 'dexie'

const db = new Dexie("tategaki")
db.version(1).stores({
  albums: "++albumId, title",
  pages: "++pageId, albumId",
  temporalPage: "pageId"
})

export default {
  getAlbums() {
    return db.albums.toCollection().toArray()
  },
  getAlbum(albumId) {
    return db.albums.get(parseInt(albumId))
  },
  async addAlbum(album) {
    delete album.albumId
    return db.albums.add({...album})

  },
  async updateAlbum(album) {
    const albumId = album.albumId
    delete album.albumId
    return db.albums.update(albumId, {...album})
  },
  deleteAlbum(albumId){
    return db.albums.delete(parseInt(albumId))
  },
  async getPages(albumId) {
    return db.pages.where({albumId: parseInt(albumId)}).toArray()
  },
  getPage(pageId) {
    return db.pages.get(parseInt(pageId))
  },
  async putPage(page) {
    if(page.pageId == null) {
      delete page.pageId
    }
    return db.pages.put({...page})
  },
  deletePage(pageId){
    console.log(`Deleting pageId of: ${pageId}`);
    return db.pages.delete(parseInt(pageId))
  },
  getTemporalPage() {
    return db.temporalPage.toCollection().toArray().then(result => result?.[0])
  },
  async putTemporalPage(page) {
    await db.temporalPage.clear().then(() => console.log(`cleared`))

    if(page.pageId == null) {
      page.pageId = 0
    }
    return db.temporalPage.put({...page})
  },
  deleteTemporalPage() {
    return db.temporalPage.clear()
  }
}