export default {
  state: {
    objectURLArray: [],
  },
  mutations: {
    pushObjectURL(state, objectURL) {state.objectURLArray.push(objectURL)},
    clearObjectURL(state) {
      state.objectURLArray.forEach(objectURL => {
        URL.revokeObjectURL(objectURL)
      })
      state.objectURLArray = []
    }
  },
}