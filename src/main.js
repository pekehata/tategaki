import { CapacitorUpdater } from '@capgo/capacitor-updater'
if(process.env.NODE_ENV === "production") {
  await CapacitorUpdater.notifyAppReady().then(() => {
    console.log("### Auto update enabled!")
  })
}

import { SafeAreaController } from '@aashu-dubey/capacitor-statusbar-safe-area';
await SafeAreaController.injectCSSVariables();

import Vue from 'vue'
import router from '@/router.js'
import store from '@/store/index.js'

// Make this.$firebase available
import Firebase from '@/firebase/index.js'
Vue.prototype.$firebase = Firebase

import db from '@/db/index.js'
Vue.prototype.$db = db

import { Purchases } from '@ionic-native/purchases'
Vue.prototype.$purchases = Purchases

import App from '@/app.vue'
import Modal from '@/components/modal.vue'
import ThreeColsView from '@/components/three-cols-view.vue'
import VerticalButton from '@/components/vertical-button.vue'
import toVTextHtml from '@/js/rotate.js'

Vue.mixin({
  components: {
    Modal, ThreeColsView, VerticalButton
  },
  methods: {
    toVTextHtml,
    async checkEntitlementStatus() {
      const purchaserInfo = await this.$purchases.getPurchaserInfo()
      return purchaserInfo.entitlements.active?.premium_purchased != null
    }
  }
})

//import CSS so that webpack can assemble them
import '@/css/app.css'

const vm = new Vue({
  router,
  store,
  el: '#app',
  template: '<app/>',
  components: {
    app: App
  }
})