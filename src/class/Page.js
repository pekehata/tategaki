import cloneDeep from 'lodash/cloneDeep'
import isEqual from 'lodash/isEqual'
export default class Page {
  albumId = null
  pageId = null
  textPlane = null
  imageArrayBuffer = null
  imageType = null
  fontSizeNum = 0.9
  fontSizeUnit = 'em'
  reversed = false
  createdAt = null

  constructor(data) {
    if(data == null) return
    this.albumId = data.albumId
    this.pageId = data.pageId
    this.textPlane = data.textPlane
    this.imageArrayBuffer = data.imageArrayBuffer
    this.imageType = data.imageType
    this.fontSizeNum = data.fontSizeNum
    this.fontSizeUnit = data.fontSizeUnit
    this.reversed = data.reversed
    this.createdAt = data.createdAt
  }

  get toObject() {
    return {
      albumId: this.albumId,
      pageId: this.pageId,
      textPlane: this.textPlane,
      imageArrayBuffer: this.imageArrayBuffer,
      imageType: this.imageType,
      fontSizeNum: this.fontSizeNum,
      fontSizeUnit: this.fontSizeUnit,
      reversed: this.reversed,
      createdAt: this.createdAt,
    }
  }

  isEqual(other) {
    const self = cloneDeep(this)
    other = cloneDeep(other)
    const selfArrayBuffer = self.imageArrayBuffer
    delete self.imageArrayBuffer
    const otherArrayBuffer = other.imageArrayBuffer
    delete other.imageArrayBuffer

    return isEqual(self, other)
      && selfArrayBuffer?.byteLength === otherArrayBuffer?.byteLength
  }
}