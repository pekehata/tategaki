export default class Album {
  albumId = null;
  title = null;
  backgroundImage = null;
  backgroundOpacity = 0;
  pages = [];
  createdAt = null;
  
  constructor(obj){
    if(obj == null) return
    this.albumId = obj.albumId
    this.title = obj.title
    this.backgroundImage = obj.backgroundImage
    this.backgroundOpacity = obj.backgroundOpacity
    this.pages = obj.pages
    this.createdAt = obj.createdAt
  }
  toObject() {
    return {
      albumId: this.albumId,
      title: this.title,
      backgroundImage: this.backgroundImage,
      backgroundOpacity: this.backgroundOpacity,
      pages: this.pages,
      createdAt: this.createdAt,
    }
  }
}