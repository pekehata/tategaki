const webpack = require('webpack');
const { VueLoaderPlugin } = require('vue-loader');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const path = require('path');
const argvs = require('yargs').argv;
const devMode = process.env.NODE_ENV === 'development' || argvs.development === 'development';

const fs = require('fs')
const packageJson = fs.readFileSync('./package.json')
const version = JSON.parse(packageJson).version || '0.0.0'

let webpackConfig = {
  mode: devMode ?  'development' : 'production',

  entry: {
    app: ['./src/main.js']
  },

  output: {
    path: path.resolve(__dirname, devMode ? 'www.dev' : 'www'),
    filename: '[name].bundle.js',
  },

  optimization: {
    removeAvailableModules: true,
    splitChunks: {
      chunks: 'all'
    },
    runtimeChunk: true,
    removeEmptyChunks: true,
    mergeDuplicateChunks: true,
    providedExports: true
  },
  
  resolve: {
    extensions: ['.js', '.vue', '.json', '.css', '.html', '.styl'],
    modules: [
      path.resolve(__dirname, 'src'),
      'node_modules'
    ],
    alias: {
      'vue$': 'vue/dist/vue.esm.js',
      '@': path.resolve(__dirname, 'src')
    }
  },

  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        include: [
          path.resolve(__dirname, 'src')
        ],
        use: [{
          loader: 'babel-loader',
          options: {
            presets: [
              [
                '@babel/preset-env',
                {
                  useBuiltIns: "usage",
                  corejs: 3
                }
              ]
            ],
          } 
        }]
      },
      {
        test: /\.vue$/,
        include: path.resolve(__dirname, 'src'),
        use: 'vue-loader'
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: 'html-loader',
            options: { minimize: true }
          }
        ]
      },
      {
        test: /\.(png|jpe?g|gif|svg|woff|woff2|ttf|eot|ico|otf)(\?\S*)?$/,
        include: path.resolve(__dirname, 'src'),
        type: 'asset/resource',
      },
      {
        test: /\.css$/,
        use: [          
          devMode ? 'style-loader' : MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: { importLoaders: 1 }
          },
          {
            loader: 'postcss-loader',
            options: { sourceMap: true }
          }
        ]
      },
      {
        test: /\.json$/,
        loader: 'json'
      }
    ]
  },
  
  // See below for dev plugin management.
  plugins: [
    new VueLoaderPlugin(),
    new MiniCssExtractPlugin({
      filename: '[name].css',
      chunkFilename: '[name].css'
    }),
    new webpack.DefinePlugin({
      'process.env.VERSION': `"${version}"`,
    }),
  ],
 
  resolveLoader: {
    modules: [ 'node_modules' ]
  },
 
  performance: {
    hints: false
  },

  experiments: {
    topLevelAwait: true
  }
};

console.log(`### devMode...: ${devMode}`)

// Development mode
if(devMode) {

  webpackConfig.devtool = 'eval';

  webpackConfig.devServer = {
    static: "www.dev",
    devMiddleware: {
      writeToDisk: true
    }
  }

  let devPlugins = [
    new HtmlWebPackPlugin({
      template: 'src/public/index.html',
      chunksSortMode: 'auto',
    })
  ];

  webpackConfig.plugins = webpackConfig.plugins.concat( devPlugins　);

} else {
  
  // Production mode
  let prodPlugins = [
    new HtmlWebPackPlugin({
      template: 'src/public/index.html',
      chunksSortMode: 'auto',
      minify: {
        caseSensitive: true,
        collapseWhitespace: true,
        conservativeCollapse: true,
        removeAttributeQuotes: true,
        removeComments: true
      }
    })
  ];
  webpackConfig.plugins = webpackConfig.plugins.concat( prodPlugins );

}

module.exports = webpackConfig;
