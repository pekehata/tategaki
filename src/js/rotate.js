export default function toVTextHtml(value) {
  if( ! value) return
  //Rotateing "<" and ">" directly will break html tag which is returned by rotate(),
  //so here we replace them with intermediate random chars first, then replace them again with sanitized chars.
  //THIS MUST BE DONE EARLIER THAN ANY OTHER ROTATION OR REPLACEMENT.
  value = value.replace(/&/g, '&amp')
              .replace(/</g, '&lt')
              .replace(/>/g, '&gt')
              .replace(/([0-9])/g, "<span>$1</span>")
  //chars to be sanitized before rotated
              .replace(/\(/g, '&#40')
              .replace(/\)/g, '&#41')
              .replace(/\[/g, '&#91')
              .replace(/=/g, '&#61')
              .replace(/-/g, '&#45')
              .replace(/_/g, '&#95')
  //chars which need not to be rotated but to be replaced
              .replace(/"(.*)"/g, '〝$1〟')
              .replace(/”(.*)”/g, '〝$1〟')
              .replace(/\n/g, '<br>')
              .replace(/ /g, '&nbsp')

  const openBrackets = ['&#40', '{', '&#91', '＜', '‹', '«']
  const closeBrackets = ['&#41', '}', ']', '＞', '›', '»']
  const otherChars = ['&#45', '&#95', '&#61', ":", ";"]
  for(var openBracket of openBrackets) {
    value = value.replace(new RegExp(openBracket,'g'), styledChar("rotated open-bracket", openBracket))
  }
  for(var closeBracket of closeBrackets) {
    value = value.replace(new RegExp(closeBracket,'g'), styledChar("rotated close-bracket", closeBracket))
  }
  for(var otherChar of otherChars) {
    value = value.replace(new RegExp(otherChar,'g'), styledChar("rotated other-char", otherChar))
  }

  value = value
              .replace(/&lt/g, styledChar("rotated open-bracket", "&lt"))
              .replace(/&gt/g, styledChar("rotated close-bracket", "&gt"))
              .replace(/<span>([1-3])<\/span><span>([0-9])<\/span>([日月])/g, styledChar("combined", "$1$2") + "$3")
              .replace(/(\!\?|！？)/g, styledChar("combined", "$1"))
  return value
}

function styledChar(cssClasses, char) {
  return "<span class='" + cssClasses + "'>" + char + "</span>"
}