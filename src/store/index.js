import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persist-indexeddb'

import auth from './modules/auth.js'
import data from './modules/data.js'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    auth,
    data
  },
  plugins: [createPersistedState()]
})