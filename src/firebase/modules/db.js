import firebase from 'firebase/app';
import 'firebase/firestore'
import store from '@/store/index.js';

// On offline mode, the promise returned by add() or set()
// will never resolve. So here we set a change listner.
const snapshotPromise = function (ref) {
  return new Promise((resolve, reject) => {
    var unsubscribe = ref.onSnapshot((doc) => {
      resolve(doc)
      unsubscribe()  // unsubscribe when resolved
    }, (error) => {
      reject(error)
    });
  });
}

export default {
  getAlbums() {
    return firebase.firestore().collection('albums').get()
      .then(snapShot => snapShot.empty ? [] : snapShot.docs.map(doc => doc.data()))
  },
  getAlbum(albumId) {
    const ref = firebase.firestore().collection('albums')

    if(store.getters.isOnline) {
      return ref
        .where('albumId', '==', parseInt(albumId))
        .get()
        .then(snapShot => snapShot.empty ? null : snapShot.docs[0].data())
    } else {
      return ref.get()
        .then(snapShot => {
          if(snapShot.empty) return null
          // docs below should have contain only one doc
          const docs = snapShot.docs.filter(doc => {
            return doc.data().albumId === parseInt(albumId)
          })
          const target = docs[0].data()
          target.docId = docs[0].id
          return target
        })
    }
  },
  async addAlbum(album) {
    let isError = false
    const newAlbumId = await firebase.firestore().collection('albums')
      .orderBy('albumId')
      .limitToLast(1)
      .get()
      .then(snapShot => {
        return snapShot.empty ? 1 : ++snapShot.docs[0].data().albumId
      })
      .catch(error => {
        console.log(`error on albumId determination: ${JSON.stringify(error, null, 2)}`);
        isError = true
        return null
      })

    if(newAlbumId == null) return Promise.reject(`Couldn't determine new albumId.`)

    const ref = firebase.firestore().collection('albums')
    album.albumId = newAlbumId
    album.createdAt = firebase.firestore.Timestamp.now()
    if(store.getters.isOnline) {
      return ref.add(album.toObject()).then(() => newAlbumId)
    } else {
      const promise = snapshotPromise(ref)
      ref.add(album.toObject())
      return promise.then(() => newAlbumId)
    }
  },
  async updateAlbum(docId, album) {
    const ref = firebase.firestore().collection('albums').doc(docId)
    if(store.getters.isOnline) {
      return ref.set(album.toObject())
    } else {
      const promise = snapshotPromise(ref)
      ref.set(album.toObject())
      return promise
    }
  },
  deleteAlbum(docId){
    const ref = firebase.firestore().collection('albums').doc(docId)
    if(store.getters.isOnline) {
      return ref.delete()
    } else {
      const promise = snapshotPromise(ref)
      ref.delete()
      return promise
    }
  },
  getPages(albumId) {
    return firebase.firestore().collection('pages')
      .where('albumId', '==', albumId)
      .get()
      .then(snapShot => snapShot.empty ? [] : snapShot.docs.map(doc => doc.data()))
  },
  getPage(albumId, pageId) {
    return firebase.firestore().collection('pages')
      .where('albumId', '==', albumId)
      .where('pageId', '==', pageId)
      .get()
      .then(snapShot => snapShot.empty ? [] : snapShot.docs.map(doc => doc.data()))
  },
  addPage(page) {
    return firebase.firestore().collection('pages')
      .add(page.toObject())
  }
}