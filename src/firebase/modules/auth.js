import firebase from 'firebase/app';
import 'firebase/auth'
import store from '@/store/index.js'

export default {
  async initialize() {
    await firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL)
    firebase.auth().onAuthStateChanged(user =>{
      if(user) {
        console.log(`login status changed to: LoggedIn.`);
        store.commit('changeAuthUser', user)
      } else {
        console.log(`login status changed to: LoggedOut.`);
        store.commit('changeAuthUser', null)
      }
    })

    if(firebase.auth().currentUser == null) {
      await firebase.auth().signInAnonymously().then(result => {
        console.log(`logged in anonymously: ${result.user.uid}`);
      })
    }
  },
}