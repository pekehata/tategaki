import firebase from 'firebase/app';
// import 'firebase/auth'
// import 'firebase/firestore'
// import 'firebase/storage'
import 'firebase/analytics'

// import auth from '@/firebase/modules/auth.js'
// import db from '@/firebase/modules/db.js'

// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyCXTVCN4e-k6PbF-5Jge6HPrRP--TRh6m0",
  authDomain: "tategaki-app.firebaseapp.com",
  databaseURL: "https://tategaki-app.firebaseio.com",
  projectId: "tategaki-app",
  storageBucket: "tategaki-app.appspot.com",
  messagingSenderId: "646993593712",
  appId: "1:646993593712:web:8b0263d7600c0c4a6a3bb5",
  measurementId: "G-JCYZZWWY65"
}

// Initialize Firebase
firebase.initializeApp(firebaseConfig)

// // firestore
// // *** MUST BE THE FIRST ***
// firebase.firestore().settings({
//   merge: true,
//   cacheSizeBytes: firebase.firestore.CACHE_SIZE_UNLIMITED,
// })
// // enable offline cache
// await firebase.firestore().enablePersistence({synchronizeTabs:true, experimentalForceOwningTab:false})
//   .catch(error1 => {
//     console.log(`error on firebase.firestore().enablePersistence(). message: ${JSON.stringify(error1)}`)
//     console.log(`Retrying firebase.firestore().enablePersistence()...`)
//     // Try again...
//     firebase.firestore().enablePersistence().catch(error2 => {
//       console.log(`Failed on second try of firebase.firestore().enablePersistence() again. message: ${JSON.stringify(error2)}`)
//     })
//   })
// await firebase.firestore().disableNetwork()    // go offline

// analytics
firebase.analytics().setAnalyticsCollectionEnabled(true)

// auth
// await auth.initialize()

export default {
  // auth, db
}