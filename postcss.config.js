module.exports = ({ webpackLoaderContext }) => ({
  parser: false,
  plugins: {
    'postcss-import': { root: webpackLoaderContext.context },
    'postcss-url': {},
    'postcss-preset-env': {
      autoprefixer: {}
    },
    'cssnano': {},
  }
});
