import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store/index.js'

import Splash from '@/views/splash.vue'
import AlbumList from '@/views/album-list.vue'
import AlbumEditor from '@/views/album-editor.vue'
import PageList from '@/views/page-list.vue'
import ImageEditor from '@/views/image-editor.vue'
import PageEditor from '@/views/page-editor.vue'
import TextEditor from '@/views/text-editor.vue'
import Slideshow from '@/views/slideshow.vue'
import Settings from '@/views/settings.vue'

Vue.use(VueRouter)

const router = new VueRouter({
  routes: [
    { path: '/', component: Splash },
    { path: '/album/list', component: AlbumList },
    { path: '/album/new', component: AlbumEditor, props:{mode: 'new'} },
    { path: '/album/:albumId', props: true },
    { path: '/album/:albumId/edit', component: AlbumEditor, props: route => ({albumId: route.params.albumId, mode: 'edit'}) },
    { path: '/album/:albumId/page/list', component: PageList, props: true },
    { path: '/album/:albumId/page/new', component: PageEditor, props: route => ({albumId: route.params.albumId, mode: 'new'}) },
    { path: '/album/:albumId/page/new/image', component: ImageEditor, props: true  },
    { path: '/album/:albumId/page/new/text', component: TextEditor, props: true  },
    { path: '/album/:albumId/page/:pageId', component: PageEditor, props: true  },
    { path: '/album/:albumId/page/:pageId/image', component: ImageEditor, props: true  },
    { path: '/album/:albumId/page/:pageId/text', component: TextEditor, props: true  },
    { path: '/album/:albumId/slideshow', component: Slideshow, props: true  },
    { path: '/settings', component: Settings },
  ]
})

let firstRendering = true
router.beforeEach((to, from, next) => {
  store.commit('clearObjectURL')
  // Avoid going back to splash
  if(to.path === '/') {
    if( ! firstRendering) return
    firstRendering = false
  }
  next()
})

export default router